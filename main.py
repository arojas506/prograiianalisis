__author__ = 'aleja'
from Problem import Problem
from GeneticAlgorithm import GeneticAlgorithm
from VertexCover import VertexCover
import sys

#python main.py TestFiles\\data_gt1.txt sdfas fasdf asdfas dffd fasdf asdf
#mincover.txt datos.txt 100 1000 3 2 output.txt


problem_file = ""
population_config_file = ""
generation_cycles = 0
population_size_arg = 0
mutation_percentage = 0
politic_method = 0
output_file = ""

def main():
    quit = False
    global  problem_file, population_config_file, generation_cycles, population_size_arg, mutation_percentage, politic_method, output_file
    while not quit:
        if len(sys.argv) == 8:
            #try:
            problem_file = sys.argv[1]
            population_config_file = sys.argv[2]
            generation_cycles = int(sys.argv[3])
            population_size_arg = int(sys.argv[4])
            mutation_percentage = float(sys.argv[5])
            politic_method = int(sys.argv[6])
            output_file = sys.argv[7]
            parse_arguments()
            """
            except:
                quit = True
                print 'Fatal Error while parsing'
                print 'Re-run the application correctly'
            """

            quit = True

        else:
            quit = True
            print 'Wrong number of arguments :', len(sys.argv)-1, '; Expected 7'
            print 'Re-run the application correctly'

def parse_arguments():
    global problem_file, population_config_file, generation_cycles, population_size_arg, mutation_percentage, politic_method, output_file

    problem = Problem()
    problem_declared = problem.readProblem(problem_file)

    if ord(u"GT1"[0]) == ord(problem_declared[0]) \
        and ord(u"GT1"[1]) == ord(problem_declared[1]) \
        and ord(u"GT1"[2]) == ord(problem_declared[2]):

        print "GT1 Detected"
        vertexCover = VertexCover("GT1")
        vertexCover.readProblem(problem_file)
        print "Data loaded into vertex cover"
        genetic_algorithm = GeneticAlgorithm(politic_method, 1, mutation_percentage, population_size_arg, vertexCover)
        print "Genetic Algorithm Created"
        genetic_algorithm.readPopulation(population_config_file)
        print "Population File Parsed"
        print genetic_algorithm.getBestFitness()





    if ord(u"SP5"[0]) == ord(problem_declared[0]) \
        and ord(u"SP5"[1]) == ord(problem_declared[1]) \
        and ord(u"SP5"[2]) == ord(problem_declared[2]):

        print "SP5 DETECTED"




main()