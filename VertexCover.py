__author__ = 'aleja'
import codecs

class VertexCover:


    def __init__(self, name):
        self.genSize = 0
        self.name = name
        self.vertexAmount = 0
        self.arcAmount = 0
        self.desiredCover = 0
        self.arcConnections = []

    def addArcConnection(self, value):
        values = value.split(" ")
        self.arcConnections.append([int(values[0]), int(values[0])])

    def readProblem(self, filename):

        lines = ""
        f = codecs.open(filename, encoding='mbcs')
        for line in f:
            lines = lines+line.encode("utf-8")
        f.close()
        lines = lines.split("\n")
        for index in range(len(lines)):
            if index == 1:
                #Un nuumero N indicando la cantidad de vertices
                self.vertexAmount = int(lines[index])
            if index == 2:
                #Un nuumero M indicando la cantidad de aristas
                self.arcAmount = int(lines[index])
            if index == 3:
                #Un nuumero K indicando el tamano del recubrimiento deseado
                self.desiredCover = int(lines[index])
            else:
                if index > 3:
                    self.addArcConnection(lines[index])

    def fitness(self, gen):
        result = self.desiredCover
        found = False
        for indexA in range(len(gen)):
            if gen[indexA] == 0:
                indexB = 0
                while indexB < len(self.arcConnections[indexB]):
                    if indexA != indexB:
                        indexC = 0
                        while indexC < len(self.arcConnections[indexB][indexC]):
                            if indexA == self.arcConnections[indexB][indexC]:
                                if result > 1:
                                    result -= 1
                            indexC =+ 1


                    else:
                        if result > 1:
                            result -= 1
                    indexB += 1
            if gen[indexA] == 1:
                indexB = 0
                while indexB < len(self.arcConnections[indexB]):
                    if indexA == indexB:
                        indexC = 0
                        while indexC < len(self.arcConnections[indexB][indexC]):
                            if indexA == self.arcConnections[indexB][indexC]:
                                found = True
                            indexC =+ 1

                        if found is False:
                            result -= 1
                    indexB += 1

        return result

    def getArcConnections(self):
        return self.arcConnections

    def getDesiredCover(self):
        return self.desiredCover

    def getArcAmount(self):
        return self.arcAmount

    def getVertexAmount(self):
        return self.vertexAmount

    def getGenSize(self):
        return self.genSize

    def getProblemName(self):
        return self.problemName