__author__ = 'aleja'
import codecs


class Problem:

    def __init__(self):
        self.genSize = 0
        self.problemName = ""

    def readProblem(self, filename):
        lines = ""
        f = codecs.open(filename, encoding='mbcs')
        for line in f:
            lines = lines+line.encode("utf-8")
        f.close()
        lines = lines.split("\n")
        return lines[0]

    def fitness(self, gen):
        return 0

    def getGenSize(self):
        return self.genSize

    def getProblemName(self):
        return self.problemName


