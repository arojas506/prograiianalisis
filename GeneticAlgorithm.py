import random
import codecs

class GeneticAlgorithm:

    def __init__(self, politic, crosses, mutation, population, problem):
        self.politic = politic
        self.CrossesNumber = crosses
        self.mutation = mutation
        self.population = population
        self.problem = problem
        self.genesList = []
        self.geneSize = 0

    def resetPopulation(self, newSize):
        self.population = newSize

    def readPopulation(self, filename):
        lines = ""
        f = codecs.open(filename, encoding='mbcs')
        for line in f:
            lines = lines+line.encode("utf-8")
        f.close()
        lines = lines.split("\r\n")
        for lineIndex in range(len(lines)):
            if lineIndex == 1:
                self.geneSize = int(lines[lineIndex])
            if lineIndex > 1:
                gen = list(lines[lineIndex])
                self.genesList.append([int(i) for i in gen])


    def getBestFitness(self):
        currentBest = self.problem.fitness(self.genesList[0])
        bestIndex = 0
        for genesIndex in range(self.geneSize):
            print self.problem.fitness(self.genesList[genesIndex])
            if currentBest < self.problem.fitness(self.genesList[genesIndex]):
                bestIndex = genesIndex
        return self.genesList[bestIndex]


    #TODO
    def writePopulation(self, filename):
        content = self.problem.getProblemName() + "\n"
        if self.problem.getProblemName() == "GT1":
            pass
        if self.problem.getProblemName() == "SP5":
            pass
        fileoutput = codecs.open(filename, "a", encoding='mbcs')
        fileoutput.write(content.decode('utf-8'));
        fileoutput.close()



